// Require our dependencies
var path = require('path')
var webpack = require('webpack')
var BundleTracker = require('webpack-bundle-tracker')

module.exports = {
	// The base directory (absolute path) for resolving the entry option
	context: __dirname,
	// The entry point. Extensions will be specified later in the 'resolve' section
	entry: [
		'./client/js/client'
	],

	output: {
		// Where the compiled bundle will be stored
		path: path.resolve('./client/bundles/'),
		// Naming convention webpack should use for the files
		filename: 'client.min.js',
	},

	plugins: [
		// Tells webpack where to store data about the bundle
		new BundleTracker({filename: './webpack-stats.json'}),
		// Makes jQuery available in every module
		new webpack.ProvidePlugin({
			$: 'jquery',
			jQuery: 'jquery',
			'window.jQuery': 'jquery'
		})
	],

	module: {
		loaders: [
			// A regex that tells webpack to use the following loaders on all .js and .jsx files
			{test: /\.jsx?$/,
				// Exclude node_modules from babel transpile
				exclude: /node_modules/,
				// Use the babel loader
				loader: 'babel-loader',
				query: {
					// Specify that we will use React code
					presets: ['react', 'es2015', 'stage-0'],
					plugins: ['react-html-attrs', 'transform-class-properties', 'transform-decorators-legacy'],
				}
			}
		]
	},

	resolve: {
		// Tells webpack where to look for modules
		modulesDirectories: ['node_modules'],
		// Extensions that should be used to resolve modules
		extensions: ['', '.js', '.jsx']
	}
}