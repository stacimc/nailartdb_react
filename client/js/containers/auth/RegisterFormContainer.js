import RegisterForm from '../../components/auth/RegisterForm.js';
import { registerUser, registerUserSuccess, registerUserFailure, resetUser }
      from '../../actions/users';

import { reduxForm } from 'redux-form';
import { connect } from 'react-redux'


function validate(values) {
  var errors = {};
  var hasErrors = false;

  if (!values.username || values.username.trim() === '') {
    errors.username = 'Enter username';
    hasErrors = true;
  }
  if(!values.email || values.email.trim() === '') {
    errors.email = 'Enter email';
    hasErrors = true;
  }
  if(!values.password || values.password.trim() === '') {
    errors.password = 'Enter password';
    hasErrors = true;
  }
  if(!values.confirmPwd || values.confirmPwd.trim() === '') {
    errors.confirmPwd = 'Confirm Password';
    hasErrors = true;
  }

  if(values.password  && values.password.trim() !== '' 
    && values.confirmPwd  && values.confirmPwd.trim() !== '' 
    && values.password !== values.confirmPwd) {
    errors.confirmPwd = 'Passwords don\'t match';
    hasErrors = true;
  }
   return hasErrors && errors;
} 

const mapDispatchToProps = (dispatch) => {
  return {
   registerUser: (values) => {
      return new Promise((resolve, reject) => {
       dispatch(registerUser(values))
        .then((response) => {
          console.log("RESPONSE", response)
            let data = response.payload.data;

            if(response.payload.status != 201) {
              dispatch(registerUserFailure(response.payload));
              reject(data);
             } else {
              localStorage.token = response.payload.data.token;
              dispatch(registerUserSuccess(response.payload)); 
              resolve();
            }
          });
      });
   }
  }
}


function mapStateToProps(state, ownProps) {
  return { 
    user: state.user,
    validateFields: state.validateFields
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(RegisterForm);