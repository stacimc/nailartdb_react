import LoginForm from '../../components/auth/LoginForm.js';
import {loginUser, loginUserSuccess, loginUserFailure, resetUserFields } from '../../actions/users';
import { reduxForm, SubmissionError } from 'redux-form';
import { connect } from 'react-redux'


const mapDispatchToProps = (dispatch) => {
  return {
   loginUser: (values) => {
     return new Promise((resolve, reject) => {
      console.log("logging in user with values", values);
     dispatch(loginUser(values))
      .then((response) => {
        console.log("RESPONSE", response);
          let data = response.payload.data;
          //if any one of these exist, then there is a field error 
          if(response.payload.status != 200) {
            dispatch(loginUserFailure(response.payload));
            // throw {username: '', _error:'Login failed'}
           } else {
            localStorage.token = response.payload.data.token;
            console.log("TOKEN:", localStorage.token)
            dispatch(loginUserSuccess(response.payload));
          }
        });
      });
   }
  }
}

function mapStateToProps(state, ownProps) {
  return { 
    user: state.user,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm)