import React, { Component } from 'react';
import { connect } from 'react-redux';
import { userFromToken, userFromTokenSuccess, userFromTokenFailure, resetToken } from '../../actions/users';
import Layout from '../../components/layout/Layout.js';

const mapDispatchToProps = (dispatch) => {
  return {
  	 loadUserFromToken: () => {
  	 	console.log("LOADING USER FROM TOKEN");
  	 	let token = localStorage.token;
  	 	console.log("TOKEN", token);
  	 	if(!token || token === '') {
  	 		return;
  	 	}

	    dispatch(userFromToken(token))
	    .then((response) => {
	        if (!response.error) {
	        	console.log("SUCCESS GETTING USER");
	          	localStorage.token = response.payload.data.token;
	            dispatch(userFromTokenSuccess(response.payload))
	        } else {
	        	console.log("ERROR GETTING USER")
	            delete localStorage.token;
	            dispatch(userFromTokenFailure(response.payload));
	        }
	    });
  	 },
    resetMe: () =>{
    	delete localStorage.token;
    	dispatch(resetToken());
    }
  }
}


export default connect(null, mapDispatchToProps)(Layout);