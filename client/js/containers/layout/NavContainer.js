import React from "react";
import { connect } from "react-redux";
import { logoutUser } from "../../actions/users.js"
import Nav from "../../components/layout/Nav.js"
// import { IndexLink, Link } from "react-router";
// import { loggedIn, logout } from '../../auth.js'

function mapStateToProps(state) {
	return {
		authenticatedUser: state.user.status === 'authenticated' ? state.user.user : null,
		user: state.user
	};
}

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		logout: () => {
			delete localStorage.token;
			dispatch(logoutUser());
		}
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Nav);