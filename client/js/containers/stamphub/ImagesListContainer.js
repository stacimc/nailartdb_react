import { connect } from 'react-redux'
import { fetchImages, fetchImagesSuccess, fetchImagesFailure, filterImages,
	fetchPlateImages, fetchPlateImagesSuccess, fetchPlateImagesFailure, filterPlateImages }
	from '../../actions/stamphub/images';

import ImagesList from '../../components/stamphub/ImagesList'

const mapStateToProps = (state, ownProps) => {
	var searchtext = new RegExp(state.images.imagesList.searchtext, "i");
	var images = state.images.imagesList.images;

	return {
		imagesList: state.images.imagesList,
		filteredImages: images.filter((image) => image.tags.search(searchtext) > -1)
	}

}

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		fetchImages: () => {
			// Fetch all images if props.plate_id is null, else fetch images for that plate
			if (ownProps.plate_id == null){
				dispatch(fetchImages()).then((response) => {
					!response.error ? dispatch(fetchImagesSuccess(response.payload)) : dispatch(fetchImagesFailure(response.payload));
				});
			} else {
				dispatch(fetchPlateImages(ownProps.plate_id)).then((response) => {
					!response.error ? dispatch(fetchPlateImagesSuccess(response.payload)) : dispatch(fetchPlateImagesFailure(response.payload));
				})
			}
			
		},
		onSearch: (searchtext) => {
			dispatch(filterImages(searchtext));
		}
		
	}
}



export default connect(mapStateToProps, mapDispatchToProps)(ImagesList)