import { connect } from 'react-redux'
import { fetchPlate, fetchPlateSuccess, fetchPlateFailure }
		from '../../actions/stamphub/plates';


import PlateDetail from '../../components/stamphub/PlateDetail'

const mapStateToProps = (state, ownProps) => {
	return {
		activePlate: state.plates.activePlate,
		plateId: ownProps.id,
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		fetchPlate: (id) => {
			dispatch(fetchPlate(id)).then((response) => {
				!response.error ? dispatch(fetchPlateSuccess(response.payload)) : dispatch(fetchPlateFailure(response.payload));
			});
		},		
	}
}



export default connect(mapStateToProps, mapDispatchToProps)(PlateDetail)