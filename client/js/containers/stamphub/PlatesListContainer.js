import { connect } from 'react-redux'
import { fetchPlates, fetchPlatesSuccess, fetchPlatesFailure, filterPlates } from '../../actions/stamphub/plates';


import PlatesList from '../../components/stamphub/PlatesList'

const mapStateToProps = (state) => {
	var searchtext = new RegExp(state.plates.platesList.searchtext, "i");
	var plates = state.plates.platesList.plates;

	return {
		platesList: state.plates.platesList,
		filteredPlates: plates.filter((plate) => plate.number.search(searchtext) > -1)
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		fetchPlates: () => {
			dispatch(fetchPlates()).then((response) => {
				!response.error ? dispatch(fetchPlatesSuccess(response.payload)) : dispatch(fetchPlatesFailure(response.payload));
			});
		},
		onSearch: (searchtext) => {
			dispatch(filterPlates(searchtext));
		}
		
	}
}



export default connect(mapStateToProps, mapDispatchToProps)(PlatesList)