import {RESET_TOKEN,
  REGISTER_USER, REGISTER_USER_SUCCESS, REGISTER_USER_FAILURE, RESET_USER,
  LOGIN_USER, LOGIN_USER_SUCCESS,  LOGIN_USER_FAILURE,
  USER_FROM_TOKEN, USER_FROM_TOKEN_SUCCESS, USER_FROM_TOKEN_FAILURE,
  LOGOUT_USER
} from '../actions/users';

const INITIAL_STATE = {user: null, status:null, error:null, loading: false};

export default function(state = INITIAL_STATE, action) {
  let error;
  switch(action.type) {
    
    case USER_FROM_TOKEN:
        return { ...state, user: null, status:'storage', error:null, loading: true}; 
    case USER_FROM_TOKEN_SUCCESS:
        return { ...state, user: action.payload.data, status:'authenticated', error:null, loading: false};
        error = action.payload.data || {message: action.payload.message};
        return { ...state, user: null, status:'storage', error:error, loading: false};
    case RESET_TOKEN:
        return { ...state, user: null, status:'storage', error:null, loading: false};

    case REGISTER_USER:
        return { ...state, user: null, status:'register', error:null, loading: true}; 
    case REGISTER_USER_SUCCESS:
        return { ...state, user: action.payload.data.user, status:'authenticated', error:null, loading: false};
    case REGISTER_USER_FAILURE:
        error = action.payload.data || {message: action.payload.message};
        return { ...state, user: null, status:'register', error:error, loading: false};


    case LOGIN_USER:
        return { ...state, user: null, status:'login', error:null, loading: true}; 
    case LOGIN_USER_SUCCESS:
        return { ...state, user: action.payload.data.user, status:'authenticated', error:null, loading: false};
    case LOGIN_USER_FAILURE:
        error = action.payload.data || {message: action.payload.message};      
        return { ...state, user: null, status:'login', error:error, loading: false};

    case LOGOUT_USER:
      return {...state, user:null, status:'logout', error:null, loading: false};

    case RESET_USER:
        return { ...state, user: null, status:null, error:null, loading: false};
    
    default:
        return state;
  }
}