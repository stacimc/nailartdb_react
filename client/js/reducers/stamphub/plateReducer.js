import { FETCH_PLATES, FETCH_PLATES_SUCCESS, FETCH_PLATES_FAILURE, FILTER_PLATES, 
  FETCH_PLATE, FETCH_PLATE_SUCCESS, FETCH_PLATE_FAILURE }
  from '../../actions/stamphub/plates.js'

const INITIAL_STATE = { platesList: {plates: [], error: null, loading: false, searchtext: "" },
                        activePlate: {plate: null, error: null, loading: null}, }

export default function(state=INITIAL_STATE, action) {
  let error;
  switch (action.type) {
    case FETCH_PLATES:
      return {...state, platesList: {plates:[], error: null, loading: true, searchtext:""} };
    case FETCH_PLATES_SUCCESS:
      return {...state, platesList: {plates: action.payload.data, error: null, loading: false, searchtext:""} };
    case FETCH_PLATES_FAILURE:
      return {...state, platesList: {plates: [], error: error, loading: false, searchtext:""} };
    case FILTER_PLATES:
      return {...state, platesList: {...state.platesList, searchtext:action.payload}}

    case FETCH_PLATE:
      return {...state, activePlate: {plate: null, error: null, loading: true} };
    case FETCH_PLATE_SUCCESS:
      return {...state, activePlate: {plate: action.payload.data, error: null, loading: false} };
    case FETCH_PLATE_FAILURE:
      return {...state, activePlate: {plate: null, error: action.payload.message, loading: false} };

    default:
      return state;
  }
}
