import { FETCH_IMAGES, FETCH_IMAGES_SUCCESS, FETCH_IMAGES_FAILURE, FILTER_IMAGES,
        FETCH_PLATE_IMAGES, FETCH_PLATE_IMAGES_SUCCESS, FETCH_PLATE_IMAGES_FAILURE, FILTER_PLATE_IMAGES }
        from '../../actions/stamphub/images.js'

const INITIAL_STATE = { imagesList: {images: [], error: null, loading: false, searchtext: "" }, }

export default function(state=INITIAL_STATE, action) {
  let error;
  switch (action.type) {
    case FETCH_IMAGES:
      return {...state, imagesList: {images:[], error: null, loading: true, searchtext:""} };
    case FETCH_IMAGES_SUCCESS:
      return {...state, imagesList: {images: action.payload.data, error: null, loading: false, searchtext:""} };
    case FETCH_IMAGES_FAILURE:
      return {...state, imagesList: {images: [], error: action.payload.message, loading: false, searchtext:""} };
    case FILTER_IMAGES:
      return {...state, imagesList: {...state.imagesList, searchtext:action.payload}};

    case FETCH_PLATE_IMAGES:
      return {...state, imagesList: {images: [], error: null, loading: true, searchtext: ""} };
    case FETCH_PLATE_IMAGES_SUCCESS:
      return {...state, imagesList: {images: action.payload.data, error: null, loading: false, searchtext: ""} };
    case FETCH_PLATE_IMAGES_FAILURE:
      return {...state, imagesList: {images: [], error: action.payload.message, loading: false, searchtext: ""} };
    case FILTER_PLATE_IMAGES:
      return {...state, imagesList: {...state.imagesList, searchtext: action.payload} };

    default:
      return state;
  }
}
