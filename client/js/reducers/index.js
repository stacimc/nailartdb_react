import { combineReducers } from "redux"
import { reducer as formReducer } from "redux-form"

import images from "./stamphub/imageReducer"
import plates from "./stamphub/plateReducer"
import user from "./userReducer"

export default combineReducers({
  images,
  plates,
  user,
  form: formReducer,
})