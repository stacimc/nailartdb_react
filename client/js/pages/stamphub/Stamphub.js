import React from "react";
import { connect } from "react-redux"
import StamphubNav from "../../components/stamphub/StamphubNav"


export default class Stamphub extends React.Component {

  render() {
    return(
      <div class="container">

        <StamphubNav></StamphubNav>

        <div class="row">
          <div class="col-sm-8">
            <div class="panel panel-default">
              {this.props.children}
            </div>
          </div>
        </div>

      </div>
    )
  }
}