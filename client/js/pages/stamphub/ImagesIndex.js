import React, { Component } from 'react';
import ImagesListContainer from '../../containers/stamphub/ImagesListContainer.js';

class ImagesIndex extends Component {
  render() {
    return (
      <div>
        <ImagesListContainer />
      </div>
    );
  }
}


export default ImagesIndex;