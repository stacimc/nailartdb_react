import React, { Component } from 'react';
import PlateDetailContainer from '../../containers/stamphub/PlateDetailContainer.js';

class PlatesShow extends Component {
  render() {
    return (
      <div>
        <PlateDetailContainer id={this.props.params.id} />
      </div>
    );
  }
}


export default PlatesShow;