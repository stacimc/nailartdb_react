import React, { Component } from 'react';
import PlatesListContainer from '../../containers/stamphub/PlatesListContainer.js';

class PlatesIndex extends Component {
  render() {
    return (
      <div>
        <PlatesListContainer />
      </div>
    );
  }
}


export default PlatesIndex;