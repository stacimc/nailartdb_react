import React from "react";
import { Link } from "react-router";

export default class Home extends React.Component {
  render() {

    return (
      <div>
        <div class="jumbotron container">
          <div class="row">
            <h1 class="col-md-8">NailArtDB</h1>
          </div>
          <div class="row">
            <p class="col-md-8">No more flipping through endless stacks of plates and swatches looking for the right tools! NailArtDB makes it easy to find exactly what you're looking for.</p>
          </div>
        </div>

        <div class="container">
          <h2>What Would You Like to Search?</h2>
          <div>
            <div class="col-md-4">
              <Link to="stamphub/images"><span class="fa-stack fa-4x">
                <i class="fa fa-circle fa-stack-2x"></i>
                <i class="fa fa-image fa-inverse fa-stack-1x"></i>
              </span></Link>
              <h5>Stamp-Hub</h5>
              <p>Looking for that specific image? Search our collection by brand, collection, size, type, category, and much more!</p>
            </div>
            <div class="col-md-4">
              <Link to="stamphub/images"><span class="fa-stack fa-4x">
                <i class="fa fa-circle fa-stack-2x"></i>
                <i class="fa fa-paint-brush fa-inverse fa-stack-1x"></i>
              </span></Link>
              <h5>Polish-Hub</h5>
              <p>COMING SOON: Search for polishes by brand, collection, color, nail art applications, and much more.</p>
            </div>
            <div class="col-md-4">
              <Link to="stamphub/images"><span class="fa-stack fa-4x">
                <i class="fa fa-circle fa-stack-2x"></i>
                <i class="fa fa-hand-stop-o fa-inverse fa-stack-1x"></i>
              </span></Link>
              <h5>Mani-Hub</h5>
              <p>COMING SOON: Need some inspiration? Search our collection of manicures from bloggers all over the world.</p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}