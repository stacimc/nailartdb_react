import React, { Component } from 'react'
import LoginFormContainer from '../../containers/auth/LoginFormContainer.js'

class Login extends Component {
	render() {
		return (
			<div>
				<LoginFormContainer />
			</div>
		);
	}
}

export default Login;