import React, { Component } from 'react'
import RegisterFormContainer from '../../containers/auth/RegisterFormContainer.js'

class Register extends Component {
	render() {
		return (
			<div>
				<RegisterFormContainer />
			</div>
		);
	}
}

export default Register;