import React from "react"
import { Link } from "react-router"

import Footer from "../components/layout/Footer"
import NavContainer from "../containers/layout/NavContainer"
import LayoutContainer from "../containers/layout/LayoutContainer"


export default class App extends React.Component {
	render() {

    return (
      <LayoutContainer>
        {this.props.children}
      </LayoutContainer>
    )
    // const { location } = this.props;
    // const containerStyle = {
    //   marginTop: "60px"
    // };
    // console.log("layout");
    // return (
    //   <div>

    //     <NavContainer location={location} />

    //     <div class="container" style={containerStyle}>
    //       <div class="row">
    //         <div class="col-lg-12">
    //           {this.props.children}
    //         </div>
    //       </div>
    //       <Footer/>
    //     </div>
    //   </div>

    // );
  }
}