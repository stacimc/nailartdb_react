import axios from 'axios';

//Get current user from token in localStorage
export const USER_FROM_TOKEN = 'USER_FROM_TOKEN';
export const USER_FROM_TOKEN_SUCCESS = 'USER_FROM_TOKEN_SUCCESS';
export const USER_FROM_TOKEN_FAILURE = 'USER_FROM_TOKEN_FAILURE';
export const RESET_TOKEN = 'RESET_TOKEN';

//Register User
export const REGISTER_USER = 'REGISTER_USER';
export const REGISTER_USER_SUCCESS = 'REGISTER_USER_SUCCESS';
export const REGISTER_USER_FAILURE = 'REGISTER_USER_FAILURE';
export const RESET_USER = 'RESET_USER';

//Login User
export const LOGIN_USER = 'LOGIN_USER';
export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';
export const LOGIN_USER_FAILURE = 'LOGIN_USER_FAILURE';

//log out user
export const LOGOUT_USER = 'LOGOUT_USER';

// Create a a config for adding csrf to headers of POST requests
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

var csrfToken = null;
var name = 'csrftoken';
if (document.cookie && document.cookie !== '') {
    var cookies = document.cookie.split(';');
    for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i].trim();
        // Does this cookie string begin with the name we want?
        if (cookie.substring(0, name.length + 1) === (name + '=')) {
            csrfToken = decodeURIComponent(cookie.substring(name.length + 1));
            break;
        }
    }
}
console.log("CSRFTOKEN", csrfToken)

const config = {
  headers: {
    "X-CSRFToken": csrfToken
  }
}


export function userFromToken(tokenFromStorage) {
  const request = axios.get('http://localhost:8000/rest-auth/user/');

  return {
    type: USER_FROM_TOKEN,
    payload: request
  };
}

export function userFromTokenSuccess(currentUser) {
  return {
    type: USER_FROM_TOKEN_SUCCESS,
    payload: currentUser
  };
}

export function userFromTokenFailure(error) {
  return {
    type: USER_FROM_TOKEN_FAILURE,
    payload: error
  };
}


export function resetToken() {
  return {
    type: RESET_TOKEN
  };
}


export function registerUser(formValues) {
  console.log("FORM VALUES", formValues);
  const request = axios.post('http://localhost:8000/rest-auth/registration/', formValues, config);

  return {
    type: REGISTER_USER,
    payload: request,
  };
}

export function registerUserSuccess(user) {
  return {
    type: REGISTER_USER_SUCCESS,
    payload: user
  };
}

export function registerUserFailure(error) {
  return {
    type: REGISTER_USER_FAILURE,
    payload: error
  };
}


export function resetUser() {
  return {
    type: RESET_USER,
  };
}

export function loginUser(formValues) {
  console.log("FORM VALUES", formValues);
  const request = axios.post('http://localhost:8000/rest-auth/login/', formValues, config);

  return {
    type: LOGIN_USER,
    payload: request,
  };
}

export function loginUserSuccess(user) {
  return {
    type: LOGIN_USER_SUCCESS,
    payload: user
  };
}

export function loginUserFailure(error) {
  return {
    type: LOGIN_USER_FAILURE,
    payload: error
  };
}

export function logoutUser() {
  return {
    type: LOGOUT_USER
  };
}