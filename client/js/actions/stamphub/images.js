import axios from "axios";

export const FETCH_IMAGES = 'FETCH_IMAGES'
export const FETCH_IMAGES_SUCCESS = 'FETCH_IMAGES_SUCCESS'
export const FETCH_IMAGES_FAILURE = 'FETCH_IMAGES_FAILURE'
export const FILTER_IMAGES = 'FILTER_IMAGES'

// Get the images for a plate by id
export const FETCH_PLATE_IMAGES = 'FETCH_PLATE_IMAGES'
export const FETCH_PLATE_IMAGES_SUCCESS = 'FETCH_PLATE_IMAGES_SUCCESS'
export const FETCH_PLATE_IMAGES_FAILURE = 'FETCH_PLATE_IMAGES_FAILURE'
export const FILTER_PLATE_IMAGES = 'FILTER_PLATE_IMAGES'


export function fetchImages() {
	const request = axios({
		method: 'get',
		url: 'http://localhost:8000/api/images',
		headers: []
	});

	return {
		type: FETCH_IMAGES,
		payload: request
	};
}

export function fetchImagesSuccess(images) {
	return {
		type: FETCH_IMAGES_SUCCESS,
		payload: images
	};
}

export function fetchImagesFailure(error) {
	return {
		type: FETCH_IMAGES_FAILURE,
		payload: error
	};
}

export function filterImages(searchtext) {
	return {
		type: FILTER_IMAGES,
		payload: searchtext
	}
}

export function fetchPlateImages(id) {
	const request = axios({
		method: 'get',
		url: 'http://localhost:8000/api/plates/' + id + '/images',
		headers: []
	});

	return {
		type: FETCH_PLATE_IMAGES,
		payload: request
	};
}

export function fetchPlateImagesSuccess(images) {
	return {
		type: FETCH_PLATE_IMAGES_SUCCESS,
		payload: images
	};
}

export function fetchPlateImagesFailure(error) {
	return {
		type: FETCH_PLATE_IMAGES_FAILURE,
		payload: error
	}
}

export function filterPlateImages(searchtext) {
	return {
		type: FILTER_PLATE_IMAGES,
		payload: searchtext
	}
}