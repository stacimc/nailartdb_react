import axios from "axios";

// Get all plates
export const FETCH_PLATES = 'FETCH_PLATES'
export const FETCH_PLATES_SUCCESS = 'FETCH_PLATES_SUCCESS'
export const FETCH_PLATES_FAILURE = 'FETCH_PLATES_FAILURE'
export const FILTER_PLATES = 'FILTER_PLATES'

// Get a plate by id
export const FETCH_PLATE = 'FETCH_PLATE'
export const FETCH_PLATE_SUCCESS = 'FETCH_PLATE_SUCCESS'
export const FETCH_PLATE_FAILURE = 'FETCH_PLATE_FAILURE'

export function fetchPlates() {
	const request = axios({
		method: 'get',
		url: 'http://localhost:8000/api/plates',
		headers: []
	});

	return {
		type: FETCH_PLATES,
		payload: request
	};
}

export function fetchPlatesSuccess(plates) {
	return {
		type: FETCH_PLATES_SUCCESS,
		payload: plates
	};
}

export function fetchPlatesFailure(error) {
	return {
		type: FETCH_PLATES_FAILURE,
		payload: error
	};
}

export function filterPlates(searchtext) {
	return {
		type: FILTER_PLATES,
		payload: searchtext
	};
}

export function fetchPlate(id) {
	const request = axios({
		method: 'get',
		url: 'http://localhost:8000/api/plates/' + id ,
		headers: []
	});

	return {
		type: FETCH_PLATE,
		payload: request
	};
}

export function fetchPlateSuccess(plate) {
	return {
		type: FETCH_PLATE_SUCCESS,
		payload: plate
	};
}

export function fetchPlateFailure(error) {
	return {
		type: FETCH_PLATE_FAILURE,
		payload: error
	};
}