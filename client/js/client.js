import React from "react";
import ReactDOM from "react-dom";
import { Router, Route, IndexRoute, hashHistory } from "react-router";
import { Provider } from "react-redux"

import App from "./pages/App"
import Home from "./pages/Home"
import ImagesIndex from "./pages/stamphub/ImagesIndex"
import Register from "./pages/auth/Register"
import Login from "./pages/auth/Login"
import PlatesIndex from "./pages/stamphub/PlatesIndex"
import PlatesShow from "./pages/stamphub/PlatesShow"
import Settings from "./pages/Settings"
import Stamphub from "./pages/stamphub/Stamphub"

import store from "./store"

const app = document.getElementById('app')

function requireAuth(nextState, replace) {
    if (!localStorage.token) {
        replace({ 
            pathname:'/login/',
            state: {nextPathname: '/'}
        })
    }
}

ReactDOM.render(
	<Provider store={store}>
	  <Router history={hashHistory}>
	    <Route path="/" component={App}>
	      <IndexRoute component={Home}></IndexRoute>
	      <Route path="login" name="login" component={Login}></Route>
	      <Route path="register" name="register" component={Register}></Route>
	      <Route path="stamphub" name="stamphub" component={Stamphub}>
	      	<Route path="images" name="images" component={ImagesIndex} onEnter={requireAuth}></Route>
	      	<Route path="plates" name="plates" component={PlatesIndex}></Route>
	      	<Route path="plates/:id" name="plate" component={PlatesShow}></Route>
	      </Route>
	      <Route path="settings" name="settings" component={Settings}></Route>
	    </Route>
	  </Router>
	 </Provider>,
app);
