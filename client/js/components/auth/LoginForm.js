import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import { Field, reduxForm } from 'redux-form';
import {loginUser, loginUserSuccess, loginUserFailure, resetUserFields } from '../../actions/users';

class LoginForm extends Component {
  static contextTypes = {
    router: PropTypes.object
  };

  componentWillMount() {
     // this.props.resetMe();
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.user.status === 'authenticated' && nextProps.user.user && !nextProps.user.error) {
      this.context.router.push('/');
    }

    if(nextProps.user.status === 'login' && !nextProps.user.user
      && nextProps.user.error && !this.props.user.error) {
      alert(nextProps.user.error.message);
    }
  }

  renderField({ input, label, type, meta: { touched, error } }) {
    return (
      <div>
        <label>{label}</label>
        <div>
          <input {...input} placeholder={label} type={type}/>
          {touched && error && <span>{error}</span>}
        </div>
      </div>
    )
   }

  render() {
    const {handleSubmit, submitting, user, error, pristine, reset} = this.props
    if (user.status === 'authenticated') {
      return (
        <div>Login successful</div>
      )
    }
    return (
      <div className="container">
        <form onSubmit={handleSubmit(this.props.loginUser)}>
          <Field name="username" type="text" component={this.renderField} label="Username" />
          <Field name="password" type="password" component={this.renderField} label="Password" />
          {error && <strong>{error}</strong>}
          <div>
            <button type="submit" disabled={submitting}>Log In</button>
            <button type="button" disabled={pristine || submitting} onClick={reset}>Clear</button>
          </div>
        </form>
      </div>

    );
  }
}

function validate(values) {
  var errors = {};
  var hasErrors = false;
  if (!values.username || values.username.trim() === '') {
    errors.username = 'Enter username';
    hasErrors = true;
  }
  if(!values.password || values.password.trim() === '') {
    errors.password = 'Enter password';
    hasErrors = true;
  }
   return hasErrors && errors;
} 

LoginForm = reduxForm({
  form: 'LoginForm',
  validate
})(LoginForm)

export default LoginForm;