import React, { Component, PropTypes } from 'react';

import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router';

class RegisterForm extends Component {
  static contextTypes = {
    router: PropTypes.object
  };

  componentWillMount() {
     // this.props.resetMe();
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.user.status === 'authenticated' && nextProps.user.user && !nextProps.user.error) {
      this.context.router.push('/');
    }
  }

  renderField({ input, label, type, meta: { touched, error } }) {
    return (
      <div>
        <label>{label}</label>
        <div>
          <input {...input} placeholder={label} type={type}/>
          {touched && error && <span>{error}</span>}
        </div>
      </div>
    )
   }

  render() {
    const {handleSubmit, submitting, user, error, pristine, reset} = this.props
    if (user.status === 'authenticated') {
      return (
        <div>Register successful</div>
      )
    }
    return (
      <div className="container">
        <form onSubmit={handleSubmit(this.props.registerUser)}>
          <Field name="username" type="text" component={this.renderField} label="Username" />
          <Field name="email" type="email" component={this.renderField} label="Email" />
          <Field name="password1" type="password" component={this.renderField} label="Password" />
          <Field name="password2" type="password" component={this.renderField} label="Confirm Password" />
          {error && <strong>{error}</strong>}
          <div>
            <button type="submit" disabled={submitting}>Register</button>
            <button type="button" disabled={pristine || submitting} onClick={reset}>Clear</button>
          </div>
        </form>
      </div>

    );
  }
}

function validate(values) {
  var errors = {};
  var hasErrors = false;

  if (!values.username || values.username.trim() === '') {
    errors.username = 'Enter username';
    hasErrors = true;
  }
  if(!values.email || values.email.trim() === '') {
    errors.email = 'Enter email';
    hasErrors = true;
  }
  if(!values.password1 || values.password1.trim() === '') {
    errors.password1 = 'Enter password';
    hasErrors = true;
  }
  if(!values.password2 || values.password2.trim() === '') {
    errors.password2 = 'Confirm Password';
    hasErrors = true;
  }

  if(values.password1  && values.password1.trim() !== '' 
    && values.password2  && values.password2.trim() !== '' 
    && values.password1 !== values.password2) {
    errors.password2 = 'Passwords don\'t match';
    hasErrors = true;
  }
   return hasErrors && errors;
} 

RegisterForm = reduxForm({
  form: 'RegisterForm',
  validate
})(RegisterForm)

export default RegisterForm;