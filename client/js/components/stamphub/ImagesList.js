import React from "react";
import { connect } from "react-redux"

import Image from "./Image.js"


export default class ImagesList extends React.Component {
  componentWillMount() {
    console.log("COMPONENT MOUNTING");
    this.props.fetchImages()
  }

  handleKeyUp() {
    var searchtext = document.getElementById('images_searchbox').value;
    this.props.onSearch(searchtext);
  }

  render() {
    const { images, loading, error } = this.props.imagesList;
    const { filteredImages } = this.props;
    console.log('images', this.props.imagesList.images)

    if (loading) {
      return <div className="container"><h1>Images</h1><h3>Loading...</h3></div>
    } else if (error) {
      return <div className="alert alert-danger">Error: {error.message}</div>
    }

    const mappedImages = filteredImages.map(image =>
      <Image picture_url={image.picture_url} key={image.id}></Image>
    );

    return (
      <div class="panel-body">
        <h4>Image List</h4>
        <div class="row">
          <div class="col-xs-12">
            <label for="searchbox">Search:</label>
            <input onKeyUp={this.handleKeyUp.bind(this)} type="text" id="images_searchbox" className="form-control" placeholder="Filter by..."></input>
          </div>
        </div>

        <br/>
        <div class="panel panel-default">
          <div class="panel-body">
            {mappedImages}
          </div>
        </div>
      </div>
    )
  }
}
