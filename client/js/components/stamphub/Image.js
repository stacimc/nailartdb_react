import React from "react";

export default class Image extends React.Component {
  render() {

  	var style = {
  		padding: 10,
  		border: 'double',
  		float: 'left'
  	}

    return (
      <div style={style}>
        <img src={this.props.picture_url} width="100" height="100" />
      </div>
    );
  }
}