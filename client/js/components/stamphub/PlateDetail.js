import React from "react";
import { connect } from "react-redux"

import Plate from "./Plate.js"
import ImagesListContainer from '../../containers/stamphub/ImagesListContainer.js'

export default class PlateDetail extends React.Component {
  componentWillMount() {
    console.log("PLATE DETAIL MOUNTING");
    this.props.fetchPlate(this.props.plateId)
  }

  render() {
    const { plate, images, loading, error } = this.props.activePlate;

    if (loading) {
      return <div className="container"><h1>Plate Detail</h1><h3>Loading...</h3></div>
    } else if (error) {
      return <div className="alert alert-danger">Error: {error.message}</div>
    } else if(!plate) {
      // For initial component mounting, before fetchPlate has been dispatched
      // Plate will be null so if you don't have this you'll error out when you
      // try to render {plate.number}
      return <span />
    }

    // const mappedPlates = filteredPlates.map(plate =>
    //   <Plate picture_url={plate.picture_url} key={plate.id}></Plate>
    // );
    var style = {
        paddingRight: 10
      }

    return (
      
      <div class="panel-body">
        <h4>{plate.number}</h4>
        <div class="row">
          <div class="col-md-6">
            <img src={plate.picture_url} width="200" height="200"></img>
          </div>
          <div class="col-md-6">
            <table>
              <tbody>
                <tr>
                  <th style={style}>Brand:</th>
                  <td>{plate.brand}</td>
                </tr>
                <tr>
                  <th style={style}>Collection:</th>
                  <td>{plate.collection}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div class="panel">
          <ImagesListContainer plate_id={plate.id}/>
        </div>
      </div>
    )
  }
}