import React from "react";
import { connect } from "react-redux"

import Plate from "./Plate.js"


export default class PlatesList extends React.Component {
  componentWillMount() {
    this.props.fetchPlates()
  }

  handleKeyUp() {
    var searchtext = document.getElementById('plates_searchbox').value;
    this.props.onSearch(searchtext);
  }

  render() {
    const { plates, loading, error } = this.props.platesList;
    const { filteredPlates } = this.props;

    if (loading) {
      return <div className="container"><h1>Plates</h1><h3>Loading...</h3></div>
    } else if (error) {
      return <div className="alert alert-danger">Error: {error.message}</div>
    }

    const mappedPlates = filteredPlates.map(plate =>
      <Plate picture_url={plate.picture_url} id={plate.id} key={plate.id}></Plate>
    );

    return (
      <div class="panel-body">
        <h4>Plate List</h4>
        <div class="row">
          <div class="col-xs-12">
            <label for="searchbox">Search:</label>
            <input onKeyUp={this.handleKeyUp.bind(this)} type="text" id="plates_searchbox" className="form-control" placeholder="Filter by..."></input>
          </div>
        </div>

        <br/>
        <div class="panel panel-default">
          <div class="panel-body">
            {mappedPlates}
          </div>
        </div>
      </div>
    )
  }
}