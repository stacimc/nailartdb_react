import React from "react";
import { IndexLink, Link } from "react-router";

export default class Nav extends React.Component {
  constructor() {
    super()
    this.state = {
      collapsed: true,
    };
  }

  toggleCollapse() {
    const collapsed = !this.state.collapsed;
    this.setState({collapsed});
  }

  render() {

    return (
      <nav class="navbar navbar-default" role="navigation">
        <div class="navbar-header">
          <Link to='stamphub/images' class="navbar-brand">Stamp-Hub</Link>
        </div>
        <ul class="nav navbar-nav">
          <li><Link to='stamphub/images'>Images</Link></li>
          <li><Link to='stamphub/plates'>Plates</Link></li>
          <li><Link to='stamphub/images'>Collections</Link></li>
          <li><Link to='stamphub/images'>Brands</Link></li>
        </ul>
      </nav>
    );
  }
}