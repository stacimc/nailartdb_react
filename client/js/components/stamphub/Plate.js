import React from "react";

import { Link } from "react-router";

export default class Plate extends React.Component {
  render() {

  	var style = {
  		padding: 10,
  		border: 'double',
  		float: 'left'
  	}

    return (
      <div style={style}>
        <Link to={"stamphub/plates/" + this.props.id}>
          <img src={this.props.picture_url} width="200" height="200" />
        </Link>
      </div>
    );
  }
}