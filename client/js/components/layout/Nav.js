import React, { PropTypes} from "react";
import { IndexLink, Link } from "react-router";

export default class Nav extends React.Component {
  static contextTypes = {
    router: PropTypes.object
  };

  componentWillUnmount() {
     // this.props.resetMe();
  }

  componentWillReceiveProps(nextProps) {
    if(this.props.user.user && !nextProps.user.user) {
      this.context.router.push('/');
    }
  }
  
  renderAuthButtons(authenticatedUser) {
    if (authenticatedUser) {
      return (
        <ul class="nav navbar-nav navbar-right">
          <li>
            <Link to='/'>{authenticatedUser.username}</Link>
          </li>
          <li>
            <Link to='/' onClick={this.props.logout}>Logout</Link>
          </li>
        </ul>
      )
    } else {
      return (
        <ul class="nav navbar-nav navbar-right">
          <li>
            <Link to='/login'>Login</Link>
          </li>
          <li>
            <Link to='/register'>Register</Link>
          </li>
        </ul>
      )
    }
  }

  render() {
    const { location, authenticatedUser, user } = this.props;
    console.log('props', this.props)

    return (
      <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <ul class="nav navbar-nav">
              <li >
                <IndexLink to="/">NailArtDB</IndexLink>
              </li>
              <li >
                <Link to="stamphub/images">Stamp-Hub</Link>
              </li>
              <li >
                <Link to="settings">Settings</Link>
              </li>
            </ul>
            {this.renderAuthButtons(authenticatedUser)}
        </div>
      </nav>
    );
  }
}