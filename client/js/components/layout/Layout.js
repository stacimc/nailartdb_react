import React from "react"
import { Link } from "react-router"

import Footer from "../../components/layout/Footer"
import NavContainer from "../../containers/layout/NavContainer"


export default class Layout extends React.Component {
	componentWillMount() {
		this.props.loadUserFromToken();
	}
	
	render() {
	    const { location } = this.props;
	    const containerStyle = {
	      marginTop: "60px"
	    };
	    console.log("layout");
	    return (
	      <div>

	        <NavContainer location={location} />

	        <div class="container" style={containerStyle}>
	          <div class="row">
	            <div class="col-lg-12">
	              {this.props.children}
	            </div>
	          </div>
	          <Footer/>
	        </div>
	      </div>

	    );
  	}
}