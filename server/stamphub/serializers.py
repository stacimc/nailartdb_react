from rest_framework import serializers

from .models import PlateBrand, PlateCollection, Plate, Image, Tag
from .models import PLATE_SHAPE_CHOICES

class TagSerializer(serializers.ModelSerializer):
	class Meta:
		model = Tag

class ImageSerializer(serializers.ModelSerializer):
	tags = serializers.SerializerMethodField()
	picture_url = serializers.SerializerMethodField()
	plate_number = serializers.SerializerMethodField()

	class Meta:
		model = Image
		fields = ('id', 'tags', 'img_type', 'size', 'picture_url', 'plate', 'plate_number')

	def get_tags(self, obj):
		tags = obj.tags.all()
		tagnames = [tag.name for tag in tags]
		return (" ").join(tagnames)

	def get_picture_url(self, obj):
		return obj.picture.url

	def get_plate_number(self, obj):
		return obj.plate.number

class PlateSerializer(serializers.ModelSerializer):
	picture_url = serializers.SerializerMethodField()
	brand = serializers.SerializerMethodField()
	collection = serializers.StringRelatedField()

	class Meta:
		model = Plate
		fields = ('id', 'number', 'picture_url', 'picture_credit', 'collection', 'brand')

	def get_picture_url(self, obj):
		return obj.picture.url

	def get_brand(self, obj):
		return obj.collection.brand.name

class PlateCollectionSerializer(serializers.ModelSerializer):
	brand = serializers.StringRelatedField()
	plate_shape = serializers.SerializerMethodField()
	full_name = serializers.SerializerMethodField()

	class Meta:
		model = PlateCollection
		fields = ('id', 'name', 'brand', 'notes', 'review_url', 'plate_shape', 'full_name')

	def get_plate_shape(self, obj):
		return obj.get_plate_shape_display()

	def get_full_name(self, obj):
		return obj.name + ' ' + obj.brand.name

class PlateBrandSerializer(serializers.ModelSerializer):
	num_collections = serializers.SerializerMethodField()

	class Meta:
		model = PlateBrand
		fields = ('id', 'name', 'preferred', 'website', 'creator', 'num_collections')

	def get_num_collections(self, obj):
		return len(obj.platecollection_set.all())