from django.shortcuts import render

from rest_framework import generics, permissions

from .serializers import TagSerializer, ImageSerializer, PlateSerializer, PlateCollectionSerializer, PlateBrandSerializer
from .models import PlateBrand, PlateCollection, Plate, Image, Tag, ImageTagging, ImageRating


# Use django to render the index
def index(request):
	return render(request, 'index.html')

# Image API
class ImageList(generics.ListCreateAPIView):
	model = Image
	queryset = Image.objects.all()
	serializer_class = ImageSerializer
	permission_classes = [
		permissions.AllowAny
	]

class ImageDetail(generics.RetrieveAPIView):
	model = Image
	queryset = Image.objects.all()
	serializer_class = ImageSerializer

# Plate API
class PlateList(generics.ListCreateAPIView):
	model = Plate
	queryset = Plate.objects.all()
	serializer_class = PlateSerializer
	permission_classes = [
		permissions.AllowAny
	]

class PlateDetail(generics.RetrieveAPIView):
	model = Plate
	queryset = Plate.objects.all()
	serializer_class = PlateSerializer

class PlateImageList(generics.ListAPIView):
	model = Image
	queryset = Image.objects.all()
	serializer_class = ImageSerializer

	def get_queryset(self):
		queryset = super(PlateImageList, self).get_queryset()
		return queryset.filter(plate__pk=self.kwargs.get('pk'))

# PlateCollection API
class PlateCollectionList(generics.ListCreateAPIView):
	model = PlateCollection
	queryset = PlateCollection.objects.all()
	serializer_class = PlateCollectionSerializer
	permission_classes = [
		permissions.AllowAny
	]

class PlateCollectionDetail(generics.RetrieveAPIView):
	model = PlateCollection
	queryset = PlateCollection.objects.all()
	serializer_class = PlateCollectionSerializer

class PlateCollectionPlateList(generics.ListAPIView):
	model = Plate
	queryset = Plate.objects.all()
	serializer_class = PlateSerializer

	def get_queryset(self):
		queryset = super(PlateCollectionPlateList, self).get_queryset()
		return queryset.filter(collection__pk=self.kwargs.get('pk'))

# PlateBrand API
class PlateBrandList(generics.ListCreateAPIView):
	model = PlateBrand
	queryset = PlateBrand.objects.all()
	serializer_class = PlateBrandSerializer
	permission_classes = [
		permissions.AllowAny
	]

class PlateBrandDetail(generics.RetrieveAPIView):
	model = PlateBrand
	queryset = PlateBrand.objects.all()
	serializer_class = PlateBrandSerializer

class PlateBrandCollectionList(generics.ListAPIView):
	model = PlateCollection
	queryset = PlateCollection.objects.all()
	serializer_class = PlateCollectionSerializer

	def get_queryset(self):
		queryset = super(PlateBrandCollectionList, self).get_queryset()
		return queryset.filter(brand__pk=self.kwargs.get('pk'))