# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import stamphub.models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Image',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('img_type', models.CharField(choices=[('BUFFET', 'Buffet'), ('FULL', 'Full image'), ('HALFMOON', 'Full image with half moon'), ('INDIV', 'Individual element'), ('HALF', 'Half image, not diagonal'), ('DIAGONAL', 'Half image, diagonal'), ('FRENCH', 'French'), ('VSTRIPE', 'Vertical Stripe'), ('HSTRIPE', 'Horizontal Stripe'), ('DSTRIPE', 'Diagonal Stripe')], blank=True, max_length=10)),
                ('size', models.CharField(choices=[('L', 'Too big'), ('M', 'Perfect'), ('S', 'Too small')], blank=True, max_length=3)),
                ('picture', models.ImageField(storage=stamphub.models.OverwriteStorage(), blank=True, upload_to=stamphub.models.get_image_photo_path)),
            ],
            options={
                'verbose_name_plural': 'images',
            },
        ),
        migrations.CreateModel(
            name='ImageRating',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('quality', models.CharField(choices=[('GREAT', 'Great'), ('GOOD', 'Good'), ('BAD', 'Bad'), ('AWFUL', 'Awful')], blank=True, max_length=10)),
                ('image', models.ForeignKey(to='stamphub.Image')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='ImageTagging',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('image', models.ForeignKey(to='stamphub.Image')),
            ],
        ),
        migrations.CreateModel(
            name='Plate',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('number', models.CharField(max_length=25, unique=True)),
                ('picture', models.ImageField(storage=stamphub.models.OverwriteStorage(), blank=True, upload_to=stamphub.models.get_plate_photo_path)),
                ('picture_credit', models.CharField(null=True, max_length=45)),
            ],
            options={
                'verbose_name_plural': 'plates',
            },
        ),
        migrations.CreateModel(
            name='PlateBrand',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=40)),
                ('preferred', models.BooleanField(default=True)),
                ('website', models.URLField(null=True, blank=True)),
                ('creator', models.CharField(null=True, blank=True, max_length=50)),
            ],
            options={
                'verbose_name_plural': 'brands',
            },
        ),
        migrations.CreateModel(
            name='PlateCollection',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('notes', models.TextField(null=True, blank=True)),
                ('review_url', models.URLField(null=True, blank=True)),
                ('plate_shape', models.CharField(choices=[('C', 'circle'), ('S', 'square'), ('LR', 'long rectangle'), ('WR', 'wide rectangle'), ('H', 'hexagon'), ('O', 'octagon'), ('F', 'full page')], max_length=2)),
                ('brand', models.ForeignKey(to='stamphub.PlateBrand')),
            ],
            options={
                'verbose_name_plural': 'collections',
            },
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=20, unique=True)),
                ('n_applied', models.IntegerField(default=0)),
            ],
            options={
                'verbose_name_plural': 'tags',
            },
        ),
        migrations.AddField(
            model_name='plate',
            name='collection',
            field=models.ForeignKey(null=True, to='stamphub.PlateCollection'),
        ),
        migrations.AddField(
            model_name='plate',
            name='owners',
            field=models.ManyToManyField(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='imagetagging',
            name='tag',
            field=models.ForeignKey(to='stamphub.Tag'),
        ),
        migrations.AddField(
            model_name='imagetagging',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='image',
            name='plate',
            field=models.ForeignKey(to='stamphub.Plate'),
        ),
        migrations.AddField(
            model_name='image',
            name='tags',
            field=models.ManyToManyField(to='stamphub.Tag', through='stamphub.ImageTagging'),
        ),
    ]
