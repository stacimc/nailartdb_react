import os
from django.db import models
from django.contrib.auth.models import User
from django.core.files.storage import FileSystemStorage

class OverwriteStorage(FileSystemStorage):
	def _save(self, name, content):
		if self.exists(name):
			self.delete(name)
		return super(OverwriteStorage, self)._save(name, content)

	def get_available_name(self, name):
		return name
	
def get_plate_photo_path(instance, filename):
	ext = filename.split('.')[-1]
	if instance.collection:
		brand = instance.collection.brand.name
		collection = instance.collection.name
		if instance.number:
			number = instance.number
			new_filename = os.path.join('plate_photos', brand, collection, number, str(instance.id))
			print('1: ' + new_filename)
		else:
			new_filename = os.path.join('plate_photos', brand, collection, filename)
			print('2: ' + new_filename)
	else:
		new_filename = os.path.join('plate_photos', filename)
		print('3: ' + new_filename)

	return '{}.{}'.format(new_filename, ext)

def get_image_photo_path(instance, filename):
	ext = filename.split('.')[-1]
	if instance.plate.collection:
		brand = instance.plate.collection.brand.name
		collection = instance.plate.collection.name
		new_filename = os.path.join('image_photos', brand, collection, str(instance.id))
	else:
		new_filename = os.path.join('image_photos', filename)

	return '{}.{}'.format(new_filename, ext)


PLATE_SHAPE_CHOICES = (
	('C', 'circle'),
	('S', 'square'),
	('LR', 'long rectangle'),
	('WR', 'wide rectangle'),
	('H', 'hexagon'),
	('O', 'octagon'),
	('F', 'full page'),
)

QUALITY_CHOICES = (
	('GREAT', 'Great'),
	('GOOD', 'Good'),
	('BAD', 'Bad'),
	('AWFUL', 'Awful'),
)

IMAGE_TYPE_CHOICES = (
	('BUFFET', 'Buffet'),
	('FULL', 'Full image'),	
	('HALFMOON', 'Full image with half moon'),
	('INDIV', 'Individual element'),
	('HALF', 'Half image, not diagonal'),
	('DIAGONAL', 'Half image, diagonal'),
	('FRENCH', 'French'),
	('VSTRIPE', 'Vertical Stripe'),
	('HSTRIPE', 'Horizontal Stripe'),
	('DSTRIPE', 'Diagonal Stripe'),
)

IMAGE_SIZE_CHOICES = (
	('L', 'Too big'),
	('M', 'Perfect'),
	('S', 'Too small')
)

class PlateBrand(models.Model):
	name = models.CharField(max_length=40)
	preferred = models.BooleanField(default=True)
	website = models.URLField(max_length=200, null=True, blank=True)
	creator = models.CharField(max_length=50, null=True, blank=True)

	class Meta:
		verbose_name_plural = 'brands'

	def __str__(self):
		return u"%s" % self.name

	def delete(self):
		collections = self.collection_set.all()
		deleted_brand, created = Brand.objects.get_or_create(name='Deleted')

		for collection in collections:
			collection.brand = deleted_brand
			collection.save()
		if self.name != 'Deleted':
			super(Brand, self).delete()

		return 'brand_delete', [self.pk]

class PlateCollection(models.Model):
	name = models.CharField(max_length=100)
	brand = models.ForeignKey(PlateBrand)
	notes = models.TextField(null=True, blank=True)
	review_url = models.URLField(null=True, blank=True)
	plate_shape = models.CharField(max_length=2, choices=PLATE_SHAPE_CHOICES)

	class Meta:
		verbose_name_plural = 'collections'

	def __str__(self):
		return u"%s" % self.name

	def delete(self):
		brand = self.brand
		plates = self.plate_set.all()

		deleted_collec, created = brand.collection_set.get_or_create(name='Deleted')
		for plate in plates:
			plate.collection = deleted_collec
			plate.save()
		if self.name != 'Deleted':
			super(Collection, self).delete()

class Plate(models.Model):
	number = models.CharField(max_length=25, unique=True)
	collection = models.ForeignKey(PlateCollection, null=True)
	picture = models.ImageField(upload_to=get_plate_photo_path, storage=OverwriteStorage(), blank=True)
	picture_credit = models.CharField(max_length=45, null=True)
	owners = models.ManyToManyField(User)

	class Meta:
		verbose_name_plural = 'plates'

	def __str__(self):
		return u"%s" % self.number

	def delete(self):
		images = self.image_set.all()
		for image in images:
			image.delete()
		self.picture.delete()
		super(Plate, self).delete()

	def save(self, *args, **kwargs):
		# delete old plate photo when photo is being updated
		try:
			this = Plate.objects.get(id=self.id)
			if this.picture != self.picture:
				this.picture.delete(save=False)
				# this technically changes the images so save them as well
				for image in this.images:
					image.picture = this.picture
					image.save()
		except: pass # when new photo then we do nothing, normal case
		super(Plate, self).save(*args, **kwargs)

class Tag(models.Model):
	name = models.CharField(max_length=20, unique=True)
	n_applied = models.IntegerField(default=0)

	class Meta:
		verbose_name_plural = 'tags'

	def __str__(self):
		return u"%s" % self.name

class Image(models.Model):
	plate = models.ForeignKey(Plate)
	tags = models.ManyToManyField(Tag, through="ImageTagging")
	img_type = models.CharField(max_length=10, choices=IMAGE_TYPE_CHOICES, blank=True)
	size = models.CharField(max_length=3, choices=IMAGE_SIZE_CHOICES, blank=True)
	picture = models.ImageField(upload_to=get_image_photo_path, storage=OverwriteStorage(), blank=True)

	class Meta:
		verbose_name_plural = 'images'

	def __str__(self):
		return u"%s" % self.plate.number + " image " + str(self.id)

	def delete(self):
		for tag in self.tags.all():
			tag.n_applied -= 1
			tag.save()
			self.tags.remove(tag)
			if tag.n_applied == 0:
				tag.delete()
		super(Image, self).delete()

class ImageTagging(models.Model):
	user = models.ForeignKey(User)
	tag = models.ForeignKey(Tag, on_delete=models.CASCADE)
	image = models.ForeignKey(Image, on_delete=models.CASCADE)

	def __str__(self):
		return u"%s" % str(self.image.id)+ " " + self.tag.name

class ImageRating(models.Model):
	user = models.ForeignKey(User)
	image = models.ForeignKey(Image, on_delete=models.CASCADE)
	quality = models.CharField(max_length=10, choices=QUALITY_CHOICES, blank=True)

	def __str__(self):
		return u"%s" % str(self.image.id) + ' ' + user.username


