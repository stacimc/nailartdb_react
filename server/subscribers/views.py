from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.models import User
from rest_framework import viewsets
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

import json

from . import serializers, permissions, authenticators

class UserView(viewsets.ModelViewSet):
    serializer_class = serializers.UserSerializer
    model = User

    def get_permissions(self):
        # allow non-authenticated user to create
        return (AllowAny(),)

class AuthView(APIView):
	authentication_classes = (authenticators.QuietBasicAuthentication,)

	def post(self, request, *args, **kwargs):
		print('user: ' + request.body)
		user = authenticate(request.user)
		if user is not None:
			login(request, user)
		return Response(serializers.UserSerializer(request.user).data)

	def delete(self, request, *args, **kwargs):
		logout(request)
		return Response({})

class UserViewSet(viewsets.ModelViewSet):
	queryset = User.objects.all()
	serializer_class = serializers.UserSerializer

	def retrieve(self, request, pk=None):
		if pk == 'i':
			return Response(serializers.UserSerializer(request.user,
				context={'request':request}).data)
		return super(UserViewSet, self).retrieve(request, pk)