from rest_framework import permissions

class IsOwner(permissions.BasePermission):
    """a user can edit only their own posts"""
    def has_object_permission(self, request, view, obj):
        return request.user == obj.user