import os
from django.db import models
from django.contrib.auth.models import User
from django.core.files.storage import FileSystemStorage
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
from django.conf import settings

# This code is triggered when a new user has been created and saved to the database
# @receiver(post_save, sender=settings.AUTH_USER_MODEL)
# def create_auth_token(sender, instance=None, created=False, **kwargs):
# 	if created:
# 		Token.objects.create(user=instance)


class OverwriteStorage(FileSystemStorage):
	def _save(self, name, content):
		if self.exists(name):
			self.delete(name)
		return super(OverwriteStorage, self)._save(name, content)

	def get_available_name(self, name):
		return name

def get_profile_photo_path(instance, filename):
	ext = filename.split('.')[-1]
	if instance.user_rec.username:
		username = instance.user_rec.username
		new_filename = os.path.join('profile_photos', username)
	else:
		new_filename = os.path.join('profile_photos', filename)

	filename = '{}.{}'.format(new_filename, ext)
	return filename


class UserProfile(models.Model):
	user_rec = models.OneToOneField(User)
	prefer_limit_stampdb = models.BooleanField(default=False)
	prefer_limit_polishdb = models.BooleanField(default=False)
	prefer_limit_manidb = models.BooleanField(default=False)
	public = models.BooleanField(default=False)
	profile_pic = models.ImageField(upload_to=get_profile_photo_path, storage=OverwriteStorage(), blank=True)

	class Meta:
		verbose_name_plural = 'subscribers'

	def __str__(self):
		return u"%s" % self.user_rec.username

	def delete(self):
		self.profile_pic.delete()
		super(UserProfile, self).delete()