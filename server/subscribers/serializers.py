from django.contrib.auth.models import User
from rest_framework import serializers
from rest_auth.serializers import UserDetailsSerializer

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', )
# To extend the User model
# class UserSerializer(UserDetailsSerializer):
#     prefer_limit_stampdb = serializers.BooleanField(source='userprofile.prefer_limit_stampdb')
#     public = serializers.BooleanField(source='userprofile.public')

#     def get_profile_pic(self, obj):
#         return obj.userprofile.picture.url

#     class Meta(UserDetailsSerializer.Meta):
#         fields = UserDetailsSerializer.Meta.fields + ('prefer_limit_stampdb', 'public')


#     def update(self, instance, validated_data):
#         profile_data = validated_data.pop('userprofile', {})
#         prefer_limit_stampdb = profile_data.get('prefer_limit_stampdb')
#         public = profile_data.get('public')

#         instance = super(UserSerializer, self).update(instance, validated_data)

#         # get and update user profile
#         profile = instance.userprofile
#         if profile_data:
#             if prefer_limit_stampd:
#                 profile.prefer_limit_stampdb = prefer_limit_stampdb
#             if public:
#                 profile.public = public
#             profile.save()
#         return instance