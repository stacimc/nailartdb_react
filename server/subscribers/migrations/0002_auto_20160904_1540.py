# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import subscribers.models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('subscribers', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('prefer_limit_stampdb', models.BooleanField(default=False)),
                ('prefer_limit_polishdb', models.BooleanField(default=False)),
                ('prefer_limit_manidb', models.BooleanField(default=False)),
                ('public', models.BooleanField(default=False)),
                ('profile_pic', models.ImageField(upload_to=subscribers.models.get_profile_photo_path, blank=True, storage=subscribers.models.OverwriteStorage())),
                ('user_rec', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name_plural': 'subscribers',
            },
        ),
        migrations.RemoveField(
            model_name='subscriber',
            name='user_rec',
        ),
        migrations.DeleteModel(
            name='Subscriber',
        ),
    ]
